package katerynadanko;

public enum Operation {
    
    ADD_COFFEE ("Добавляю кофе"),

    BOIL_WATER ( "Кипячу воду"),

    ADD_CHOCOLATE ("Добавляю шоколад"),

    ADD_MILK ("Добавляю молоко"),

    ESPRESSO_DONE ("Напиток ESPRESSO готов!"),

    CAPPUCCINO_DONE ( "Напиток CAPPUCCINO готов!"),

    HOT_CHOCOLATE_DONE("Напиток HOT CHOCOLATE готов!"),

    TEA_DONE("Напиток TEA готов!");

    private String name;

    Operation (String name){this.name = name;}

    public String toString() {
        return name;
    }
}
