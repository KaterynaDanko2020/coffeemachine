package katerynadanko;

import java.util.Arrays;

public enum Drink {

    ESPRESSO(new Operation[]{Operation.ADD_COFFEE, Operation.BOIL_WATER, Operation.ESPRESSO_DONE}),
    CAPPUCCINO(new Operation[]{Operation.ADD_COFFEE, Operation.BOIL_WATER, Operation.ADD_MILK, Operation.CAPPUCCINO_DONE}),
    CHOCOLATE(new Operation[]{Operation.ADD_COFFEE, Operation.BOIL_WATER, Operation.ADD_MILK, Operation.HOT_CHOCOLATE_DONE}),
    TEA(new Operation[]{Operation.ADD_COFFEE, Operation.BOIL_WATER, Operation.ADD_MILK, Operation.TEA_DONE});

    Drink(Operation[] operations) {
        this.operations=operations;
    }

    private Operation[] operations;

    public Operation[] getOperations() {
        return operations;
    }

    @Override
    public String toString() {
        return "Готовлю напиток " +
                Arrays.toString(operations);
    }
}


