package katerynadanko;

public class CoffeeMachineImpl implements CoffeeMachine {
    public CoffeeMachineImpl() {
    }
    Drink drink;

    public Drink getDrink() {
        return drink;
    }

    @Override
     public Drink makeDrink(Drink drink) {
        return drink;
    }

    public static void main(String[] args) {
        System.out.println(new CoffeeMachineImpl().makeDrink(Drink.ESPRESSO));
        System.out.println(new CoffeeMachineImpl().makeDrink(Drink.CAPPUCCINO));
        System.out.println(new CoffeeMachineImpl().makeDrink(Drink.CHOCOLATE));
        System.out.println(new CoffeeMachineImpl().makeDrink(Drink.TEA));
    }
}


