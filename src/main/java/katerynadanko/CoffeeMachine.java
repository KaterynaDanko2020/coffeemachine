package katerynadanko;

public interface CoffeeMachine {
     public Drink makeDrink(Drink drink);
}
